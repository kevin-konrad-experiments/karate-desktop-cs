﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3
{
    class Secteur
    {
        int id;
        string emplacement;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Emplacement
        {
            get { return emplacement; }
            set { emplacement = value; }
        }

        public Secteur(int i, string e)
        {
            this.Id = i;
            this.Emplacement = e;
        }
    }

    class Club
    {
        int id;
        string nom;
        string adresse;
        string codePostal;
        string ville;
        Secteur secteur;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }

        public string CodePostal
        {
            get { return codePostal; }
            set { codePostal = value; }
        }

        public string Ville
        {
            get { return ville; }
            set { ville = value; }
        }

        public Secteur Secteur
        {
            get { return secteur; }
            set { secteur = value; }
        }

        public Club(int id)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_club_select", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("id", id);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    this.Id = reader.GetInt32("cb_id");
                    this.Nom = reader.GetString("cb_nom");
                    this.Adresse = reader.GetString("cb_adresse");
                    this.CodePostal = reader.GetString("cb_code_postal");
                    this.Ville = reader.GetString("cb_ville");
                    this.Secteur = new Secteur(reader.GetInt32("sc_id"), reader.GetString("sc_emplacement"));
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement du club n°" + this.Id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
