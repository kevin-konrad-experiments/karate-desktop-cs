﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using KarateV3.Forms;

namespace KarateV3.Classes
{
    class Utilisateur
    {
        string test;
        string licence;
        string nom;
        string prenom;
        string email;
        string password;
        int type;

        public string Licence
        {
            get { return licence; }
            set { licence = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        public Utilisateur()
        { }

        public Utilisateur(string licence)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_utilisateur_select", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("licence", licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    this.Licence = reader.GetString("us_licence");
                    this.Nom = reader.GetString("us_nom");
                    this.Prenom = reader.GetString("us_prenom");
                    this.Email = reader.GetString("us_email");
                    this.Password = reader.GetString("us_password");
                    this.Type = reader.GetInt32("ut_id");
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement de l'utilisateur n°" + this.Licence + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Connect(string password)
        {
            Form form;

            if (this.Password != password)
            {
                MessageBox.Show("Indentifiants de connexion invalides", "Connexion refusée", MessageBoxButtons.OK);
                return;
            }

            switch (this.Type)
            {
                case 1:
                    Membre membre = new Membre(this.Licence);
                    form = new MembreForm(membre);
                    break;
                case 2:
                    Entraineur entraineur = new Entraineur(this.Licence);
                    form = new EntraineurForm(entraineur);
                    break;
                case 3:
                    form = new AdminForm(this);
                    break;
                default:
                    Membre membreDefault = new Membre(this.Licence);
                    form = new MembreForm(membreDefault);
                    break;
            }

            form.Show();
            Application.OpenForms[0].Hide();
        }

        public void RecoverPassword()
        {
            if (this.Licence == null || this.Email == "" || this.Email == null)
            {
                MessageBox.Show("Ce numéro de licence est inexistant ou aucune adresse e-mail n'y est rattachée.\r\n\r\nVeuillez vérifier votre entrée ou contacter la personne en charge de l'activité karaté.", "Identifiants invalides", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                Outils.SendMail(this.Email, this.Prenom + " " + this.Nom.ToUpper(), "GSB Karaté - Votre mot de passe", "Bonjour " + this.Prenom + ",\r\n\r\nVous avez fait une demande de récupération de mot de passe sur l'application GSB Karaté.\r\nVoici vos identifiants :\r\n\r\nLicence : " + this.Licence + "\r\nMot de passe : " + this.Password);
                MessageBox.Show("Vos identifiants vous ont été envoyés. Vous les recevrez dans quelques instants à l'adresse suivante : " + this.Email + ".", "Identifiants envoyés", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Un problème est survenu lors de l'envoi de l'email, veuillez réessayer ultérieurement.\r\n\r\nErreur : " + ex.Message, "Echec de l'envoi des identifiants", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
