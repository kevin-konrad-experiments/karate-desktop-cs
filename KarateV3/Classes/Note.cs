﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Classes
{
    class Note
    {
        int valeurNote;
        Competition competition;
        Entraineur entraineur;
        Membre membre;
        int numJury;

        public int ValeurNote
        {
            get { return valeurNote; }
            set { valeurNote = value; }
        }

        public Competition Competition
        {
            get { return competition; }
            set { competition = value; }
        }

        public Entraineur Entraineur
        {
            get { return entraineur; }
            set { entraineur = value; }
        }

        public Membre Membre
        {
            get { return membre; }
            set { membre = value; }
        }

        public int NumJury
        {
            get { return numJury; }
            set { numJury = value; }
        }

        public string NomAffichage
        {
            get { return "Jury n°" +  NumJury + " : " + ValeurNote + " (" + Entraineur.Prenom + " " + Entraineur.Nom + ")"; }
        }

        public static List<Note> Get(int co_id, string mb_licence)
        {
            List<Note> liste = new List<Note>();
            MySqlCommand cmd = new MySqlCommand("SELECT nt.nt_note, nt.co_id, nt.mb_licence, ju.et_licence, ju.ju_jury FROM karate_note nt INNER JOIN karate_jury ju ON nt.et_licence = ju.et_licence AND nt.co_id = ju.co_id WHERE nt.co_id = @co_id AND nt.mb_licence = @mb_licence ORDER BY ju.ju_jury", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@co_id", co_id);
                cmd.Parameters.AddWithValue("@mb_licence", mb_licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Note note = new Note();
                    note.ValeurNote = reader.GetInt32("nt_note");
                    note.Competition = new Competition(reader.GetInt32("co_id"));
                    note.Entraineur = new Entraineur(reader.GetString("et_licence"));
                    note.Membre = new Membre(reader.GetString("mb_licence"));
                    note.NumJury = reader.GetInt32("ju_jury");
                    liste.Add(note);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement d'une note pour la compétition n°" + co_id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return liste;
        }

        public static List<Note> GetEntraineur(int co_id, string mb_licence, string licence)
        {
            List<Note> liste = new List<Note>();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM karate_note nt INNER JOIN karate_jury ju ON nt.et_licence = ju.et_licence AND nt.co_id = ju.co_id WHERE nt.mb_licence = @mb_licence AND nt.et_licence = @et_licence AND nt.co_id = @co_id", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@co_id", co_id);
                cmd.Parameters.AddWithValue("@mb_licence", mb_licence);
                cmd.Parameters.AddWithValue("@et_licence", licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Note note = new Note();
                    note.ValeurNote = reader.GetInt32("nt_note");
                    note.Competition = new Competition(reader.GetInt32("co_id"));
                    note.Entraineur = new Entraineur(reader.GetString("et_licence"));
                    note.Membre = new Membre(reader.GetString("mb_licence"));
                    note.NumJury = reader.GetInt32("ju_jury");
                    liste.Add(note);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement d'une note pour la compétition n°" + co_id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return liste;
        }

        public void Insert()
        {
            MySqlCommand cmd = new MySqlCommand("INSERT INTO karate_note VALUES (@nt_note, @co_id, @et_licence, @mb_licence)", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@nt_note", this.ValeurNote);
                cmd.Parameters.AddWithValue("@co_id", this.Competition.Id);
                cmd.Parameters.AddWithValue("@et_licence", this.Entraineur.Licence);
                cmd.Parameters.AddWithValue("@mb_licence", this.Membre.Licence);
                cmd.ExecuteNonQuery();

                MessageBox.Show("La note a été ajoutée avec succès.", "Ajout d'une note", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de l'ajout de la note.\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
