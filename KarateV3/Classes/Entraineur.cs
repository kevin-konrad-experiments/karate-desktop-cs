﻿using KarateV3.Classes;
using KarateV3.Forms;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace KarateV3
{
    class Entraineur : Utilisateur
    {
        int niveauCompetence;
        Club club;

        public int NiveauCompetence
        {
            get { return niveauCompetence; }
            set { niveauCompetence = value; }
        }

        public Club Club
        {
            get { return club; }
            set { club = value; }
        }

        public Entraineur(string licence)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_entraineur_select", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("licence", licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    base.Licence = reader.GetString("us_licence");
                    base.Nom = reader.GetString("us_nom");
                    base.Prenom = reader.GetString("us_prenom");
                    base.Email = reader.GetString("us_email");
                    base.Password = reader.GetString("us_password");
                    base.Type = reader.GetInt32("ut_id");
                    this.NiveauCompetence = reader.GetInt32("et_niveau_competence");
                    this.Club = new Club(reader.GetInt32("cb_id"));
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement de l'entraîneur n°" + base.Licence + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Update()
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_entraineur_update", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Licence", base.Licence);
                cmd.Parameters.AddWithValue("Nom", base.Nom);
                cmd.Parameters.AddWithValue("Prenom", base.Prenom);
                cmd.Parameters.AddWithValue("Email", base.Email);
                cmd.Parameters.AddWithValue("Password", base.Password);
                cmd.Parameters.AddWithValue("NiveauCompetence", this.NiveauCompetence);
                cmd.Parameters.AddWithValue("ClubId", this.Club.Id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Les informations de l'entraîneur n°" + base.Licence + " ont bien été mises à jour.", "Compte mis à jour", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de la mise à jour de l'entraîneur n°" + base.Licence + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public List<Competition> GetCompetitionsJury()
        {
            List<Competition> liste = new List<Competition>();
            MySqlCommand cmd = new MySqlCommand("SELECT co.co_id FROM karate_competition co INNER JOIN karate_jury ju ON co.co_id = ju.co_id WHERE ju.et_licence = @et_licence", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@et_licence", base.Licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    liste.Add(new Competition(reader.GetInt32("co_id")));

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement des compétitions pour lesquels l'entraîneur n°" + base.Licence + " est jury.\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return liste;
        }

        public int GetNumJury(int CompetitionId)
        {
            int NumJury = 0;
            MySqlCommand cmd = new MySqlCommand("SELECT ju_jury FROM karate_jury WHERE et_licence = @et_licence AND co_id = @co_id", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@et_licence", base.Licence);
                cmd.Parameters.AddWithValue("@co_id", CompetitionId);
                NumJury = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement des compétitions pour lesquels l'entraîneur n°" + base.Licence + " est jury.\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return NumJury;
        }
        
    }
}
