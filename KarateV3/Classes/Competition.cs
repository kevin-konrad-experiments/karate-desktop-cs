﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Classes
{
    class Kata
    {
        int id;
        string nom;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public Kata(int i, string n)
        {
            Id = i;
            Nom = n;
        }
    }

    class Competition
    {
        int id;
        DateTime date;
        Kata kata;
        Club club;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public Kata Kata
        {
            get { return kata; }
            set { kata = value; }
        }

        public Club Club
        {
            get { return club; }
            set { club = value; }
        }

        public string NomAffichage
        {
            get { return Club.Nom + " - " + Kata.Nom + " - " + Date.ToString(); }
        }

        public Competition()
        { }

        public Competition(int id)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_competition_select", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("id", id);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    this.Id = reader.GetInt32("co_id");
                    this.Date = reader.GetDateTime("co_date");
                    this.Kata = new Kata(reader.GetInt32("ka_id"), reader.GetString("ka_nom"));
                    this.Club = new Club(reader.GetInt32("cb_id"));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement de la compétition n°" + this.Id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public List<string> GetParticipantsLicence()
        {
            List<string> liste = new List<string>();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM karate_participant WHERE co_id = @co_id", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@co_id", Id);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                    liste.Add(reader.GetString("mb_licence"));
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement des participants de la compétition n°" + this.Id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return liste;
        }

        public void Insert()
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_competition_insert", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("CoDate", this.Date);
                cmd.Parameters.AddWithValue("KataId", this.Kata.Id);
                cmd.Parameters.AddWithValue("ClubId", this.Club.Id);
                
                this.Id = Convert.ToInt32(cmd.ExecuteScalar());
                MessageBox.Show("La compétition n°" + this.Id + " a été ajoutée avec succès.", "Création de compétition", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de l'ajout de la compétition.\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
