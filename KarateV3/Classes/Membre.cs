﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using KarateV3.Classes;
using KarateV3.Forms;
using System.Data;

namespace KarateV3
{
    class Statistique
    {
        Competition competition;
        decimal moyenne;

        public Competition Competition
        {
            get { return competition; }
            set { competition = value; }
        }

        public decimal Moyenne
        {
            get { return moyenne; }
            set { moyenne = value; }
        }
    }

    class Membre : Utilisateur
    {
        DateTime dateNaissance;
        string adresse;
        string codePostal;
        string ville;
        Club club;

        public DateTime DateNaissance
        {
            get { return dateNaissance; }
            set { dateNaissance = value; }
        }

        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }

        public string CodePostal
        {
            get { return codePostal; }
            set { codePostal = value; }
        }

        public string Ville
        {
            get { return ville; }
            set { ville = value; }
        }

        public Club Club
        {
            get { return club; }
            set { club = value; }
        }

        public string NomAffichage
        {
            get { return this.Prenom + " " + this.Nom.ToUpper(); }
        }

        public Membre(string licence)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_membre_select", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("licence", licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    base.Licence = reader.GetString("us_licence");
                    base.Nom = reader.GetString("us_nom");
                    base.Prenom = reader.GetString("us_prenom");
                    base.Email = reader.GetString("us_email");
                    base.Password = reader.GetString("us_password");
                    base.Type = reader.GetInt32("ut_id");
                    this.DateNaissance = reader.GetDateTime("mb_date_naissance");
                    this.Adresse = reader.GetString("mb_adresse");
                    this.CodePostal = reader.GetString("mb_code_postal");
                    this.Ville = reader.GetString("mb_ville");
                    this.Club = new Club(reader.GetInt32("cb_id"));
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors du chargement du membre n°" + base.Licence + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Update()
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_membre_update", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Licence", base.Licence);
                cmd.Parameters.AddWithValue("Nom", base.Nom);
                cmd.Parameters.AddWithValue("Prenom", base.Prenom);
                cmd.Parameters.AddWithValue("Email", base.Email);
                cmd.Parameters.AddWithValue("Password", base.Password);
                cmd.Parameters.AddWithValue("DateNaissance", this.DateNaissance);
                cmd.Parameters.AddWithValue("Adresse", this.Adresse);
                cmd.Parameters.AddWithValue("CodePostal", this.CodePostal);
                cmd.Parameters.AddWithValue("Ville", this.Ville);
                cmd.Parameters.AddWithValue("ClubId", this.Club.Id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Les informations du membre n°" + base.Licence + " ont bien été mises à jour.", "Compte mis à jour", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de la mise à jour du membre n°" + base.Licence + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Desinscrire(Competition competition)
        {
            MySqlCommand cmd = new MySqlCommand("sp_karate_participant_delete", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Licence", this.Licence);
                cmd.Parameters.AddWithValue("CoId", competition.Id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Vos êtes désinscrit de la compétition n°" + competition.Id + ".", "Désinscription compétition", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de la désinscription à la compétition n°" + competition.Id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Participer(Competition competition)
        {
            if (competition.Date < DateTime.Now)
            {
                MessageBox.Show("Désolé, les inscriptions pour cette compétition sont terminées.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (competition.GetParticipantsLicence().Contains(this.Licence))
            {
                if (MessageBox.Show("Vous êtes déjà inscrit à cette compétition !\r\nSouhaitez-vous vous désinscrire ?", "Erreur", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    this.Desinscrire(competition);

                return;
            }

            if (MessageBox.Show("Voulez-vous vous inscrire à la compétition n°" + competition.Id + " ?", "Erreur", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                return;

            MySqlCommand cmd = new MySqlCommand("sp_karate_participant_insert", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));

            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("Licence", this.Licence);
                cmd.Parameters.AddWithValue("CoId", competition.Id);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Votre inscription à la compétition n°" + competition.Id + " a été enregistrée avec succès.", "Inscription compétition", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de l'inscription à la compétition n°" + competition.Id + ".\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public List<Statistique> getStatsCompet()
        {
            List<Statistique> liste = new List<Statistique>();
            MySqlCommand cmd = new MySqlCommand("SELECT t1.co_id, CAST((SUM(t1.nt_note) / COUNT(*)) AS DECIMAL(6,1)) as note_totale FROM karate_note as t1, karate_competition as t2 WHERE t1.mb_licence = @mb_licence AND t2.co_id = t1.co_id AND t2.co_date < NOW() GROUP BY t1.co_id ORDER BY t2.co_date DESC", new MySqlConnection(Properties.Settings.Default.gsbConnectionString));
            // SELECT co_id, (SUM(nt_note) / COUNT(*)) as note_totale FROM karate_note WHERE mb_licence = @mb_licence GROUP BY co_id ORDER BY co_date
            
            try
            {
                cmd.Connection.Open();
                cmd.Prepare();
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@mb_licence", this.Licence);
                MySqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Statistique statistique = new Statistique();
                    statistique.Competition = new Competition(Convert.ToInt32(reader.GetString("co_id")));
                    statistique.Moyenne = reader.GetDecimal("note_totale");
                    liste.Add(statistique);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Une erreur est survenue lors de la récupération de vos statistiques.\r\nVeuillez contacter le service informatique.\r\n\r\nErreur " + ex.Number + " :" + ex.Message, "Erreur MySql", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                cmd.Connection.Close();
            }

            return liste;
        }
    }
}
