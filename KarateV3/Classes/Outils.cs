﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Classes
{
    class Outils
    {
        public static void SendMail(string receiverMail, string receiverName, string subject, string body)
        {
            var smtp = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("kevin.konrad@edu.itescia.fr", "cdy16m6n"),
                EnableSsl = true
            };

            smtp.Send("kevin.konrad@edu.itescia.fr", receiverMail, subject, body + "\r\n\r\nL'équipe GSB Karaté");
        }

        public static void errorBox(string message, string titre)
        {
            MessageBox.Show(message, titre, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void successBox(string message, string titre) {
            MessageBox.Show(message, titre, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
