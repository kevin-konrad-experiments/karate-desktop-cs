﻿using KarateV3.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Forms
{
    partial class EntraineurForm : Form
    {
        Entraineur entraineur;

        public Entraineur Entraineur
        {
            get { return entraineur; }
            set { entraineur = value; }
        }

        public EntraineurForm(Entraineur e)
        {
            Entraineur = e;
            InitializeComponent();
        }

        private void EntraineurForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.OpenForms[0].Show();
        }

        public EntraineurForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bool vide = false;
            foreach (Control control in groupBox1.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    if (textBox.Tag == null || (textBox.Tag != null && textBox.Tag.ToString() != "mdp"))
                    {
                        if (textBox.Text == null || textBox.Text == "")
                        {
                            vide = true;
                        }
                    }
                }
            }

            if (!vide)
            {
                if (textBox6.Text == entraineur.Password)
                {
                    entraineur.Nom = textBox1.Text;
                    entraineur.Prenom = textBox2.Text;
                    entraineur.NiveauCompetence = Convert.ToInt32(comboBox1.Text);
                    entraineur.Email = textBox3.Text;
                    if (textBox4.Text != null && textBox4.Text != "" && textBox5.Text != null && textBox5.Text != "")
                    {
                        if (textBox4.Text == textBox5.Text)
                        {
                            entraineur.Password = textBox4.Text;
                        }
                        else
                        {
                            Outils.errorBox("Le mot de passe et sa confirmation sont différents. Merci d'indiquer deux fois le même mot de passe.", "Erreur de mot de passe");
                            textBox4.Text = textBox5.Text = "";
                            return;
                        }
                    }
                    entraineur.Update();
                    textBox4.Text = textBox5.Text = textBox6.Text = "";
                }
                else
                {
                    Outils.errorBox("Mauvais mot de passe actuel. Merci de rentrer votre mot de passe actuel pour modifier vos informations.", "Erreur de mot de passe");
                    textBox6.Text = "";
                }
            }
            else
            {
                Outils.errorBox("Merci de remplir tous les champs (sauf les deux champs de nouveaux mot de passe qui sont facultatifs).", "Erreur de champs vides");
            }
        }

        private void EntraineurForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gsbDataSet1.karate_kata' table. You can move, or remove it, as needed.
            this.karate_kataTableAdapter1.Fill(this.gsbDataSet1.karate_kata);
            // TODO: This line of code loads data into the 'gsbDataSetKata.karate_kata' table. You can move, or remove it, as needed.
            this.karate_kataTableAdapter.Fill(this.gsbDataSetKata.karate_kata);
            // TODO: This line of code loads data into the 'gsbDataSet.karate_club' table. You can move, or remove it, as needed.
            this.karate_clubTableAdapter.Fill(this.gsbDataSet.karate_club);
            // TODO: This line of code loads data into the 'gsbDataSetCompetitions.karate_competition' table. You can move, or remove it, as needed.
            this.karate_competitionTableAdapter.Fill(this.gsbDataSetCompetitions.karate_competition);

            label1.Text = "Compte n°" + entraineur.Licence;
            textBox1.Text = entraineur.Nom;
            textBox2.Text = entraineur.Prenom;
            textBox3.Text = entraineur.Email;

            label11.Text = entraineur.Club.Nom;
            label15.Text = entraineur.Club.Adresse;
            label13.Text = entraineur.Club.CodePostal + " " + entraineur.Club.Ville;
            label12.Text = entraineur.Club.Secteur.Emplacement;
            comboBox1.Text = entraineur.NiveauCompetence.ToString();

            comboBox4.DisplayMember = "NomAffichage";
            comboBox4.ValueMember = "Id";

            comboBox5.DisplayMember = "NomAffichage";
            comboBox5.ValueMember = "Lience";
            comboBox5.Items.Clear();

            listBox1.DisplayMember = "NomAffichage";
            listBox1.ValueMember = "ValeurNote";

            List<Competition> competitionsJury = entraineur.GetCompetitionsJury();

            foreach (Competition competition in competitionsJury)
            {
                comboBox4.Items.Add(competition);
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Competition competition = (Competition)comboBox4.SelectedItem;
            comboBox5.Items.Clear();

            foreach (string MembreLicence in competition.GetParticipantsLicence())
            {
                comboBox5.Items.Add(new Membre(MembreLicence));
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            Competition competition = (Competition)comboBox4.SelectedItem;
            Membre membre = (Membre)comboBox5.SelectedItem;
            List<Note> NotesMembre = Note.Get(competition.Id, membre.Licence);

            listBox1.Items.Clear();

            foreach (Note note in NotesMembre)
                listBox1.Items.Add(note);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Competition competition = new Competition();
            competition.Date = dateTimePicker1.Value;
            competition.Kata = new Kata(Convert.ToInt32(comboBox2.SelectedValue), comboBox2.Text);
            competition.Club = new Club(Convert.ToInt32(comboBox3.SelectedValue));
            competition.Insert();

            this.karate_competitionTableAdapter.Fill(this.gsbDataSetCompetitions.karate_competition);
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            TabPage tab = tabControl1.TabPages[Convert.ToInt32(button.Tag) - 1];

            switch (Convert.ToInt32(button.Tag))
            {
                case 1:
                    label1.Text = "Compte n°" + entraineur.Licence;
                    label2.Text = "Éditer mes informations et voir celles de mon club.";
                    break;
                case 2:
                    label1.Text = "Compétitions";
                    label2.Text = "Voir les compétitions et en créer de nouvelles.";
                    break;
                case 3:
                    label1.Text = "Notes";
                    label2.Text = "Attribuer des notes aux participants pour les compétitions auxquelles vous avez été désigné comme jury.";
                    break;
            }

            tabControl1.SelectedTab = tab;
            tab.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int valeur = Convert.ToInt32(comboBox6.SelectedItem);
            Competition competition = (Competition)comboBox4.SelectedItem;
            Membre membre = (Membre)comboBox5.SelectedItem;
            Note note = new Note();

            if (valeur != null && competition != null && membre != null)
            {

                note.ValeurNote = valeur;
                note.Competition = competition;
                note.Entraineur = this.Entraineur;
                note.Membre = membre;
                note.NumJury = this.Entraineur.GetNumJury(competition.Id);

                List<Note> listeNotes = Note.GetEntraineur(competition.Id, membre.Licence, Entraineur.Licence);

                if (listeNotes.Count == 0)
                {
                    note.Insert();
                    listBox1.Items.Add(note);
                }
                else
                {
                    Outils.errorBox("Vous avez déjà attribué une note à ce participant pour cette compétition. Sa sentence est irrévocable ! * musique de Koh Lanta *", "Erreur");
                }
            }
            else
            {
                Outils.errorBox("Impossible d'insérer cette note.", "Erreur d'insertion");
            }

        }
    }
}
