﻿namespace KarateV3.Forms
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.karateutilisateurtypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet9 = new KarateV3.gsbDataSet9();
            this.karateutilisateurBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet8 = new KarateV3.gsbDataSet8();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.coidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kaidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.karatekataBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet6 = new KarateV3.gsbDataSet6();
            this.cbidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.karateclubBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet7 = new KarateV3.gsbDataSet7();
            this.karatecompetitionBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet5 = new KarateV3.gsbDataSet5();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.karatesecteurBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet2 = new KarateV3.gsbDataSet2();
            this.karateclubBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet3 = new KarateV3.gsbDataSet3();
            this.karatekataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSetKata = new KarateV3.gsbDataSetKata();
            this.karateclubBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSet = new KarateV3.gsbDataSet();
            this.karatecompetitionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSetCompetitionMembre = new KarateV3.gsbDataSetCompetitionMembre();
            this.karatecompetitionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gsbDataSetCompetitions = new KarateV3.gsbDataSetCompetitions();
            this.karate_competitionTableAdapter = new KarateV3.gsbDataSetCompetitionsTableAdapters.karate_competitionTableAdapter();
            this.karate_clubTableAdapter = new KarateV3.gsbDataSetTableAdapters.karate_clubTableAdapter();
            this.karate_kataTableAdapter = new KarateV3.gsbDataSetKataTableAdapters.karate_kataTableAdapter();
            this.karate_competitionTableAdapter1 = new KarateV3.gsbDataSetCompetitionMembreTableAdapters.karate_competitionTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.karate_secteurTableAdapter = new KarateV3.gsbDataSet2TableAdapters.karate_secteurTableAdapter();
            this.karate_clubTableAdapter1 = new KarateV3.gsbDataSet3TableAdapters.karate_clubTableAdapter();
            this.gsbDataSet4 = new KarateV3.gsbDataSet4();
            this.karatecompetitionBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.karate_competitionTableAdapter2 = new KarateV3.gsbDataSet4TableAdapters.karate_competitionTableAdapter();
            this.karate_competitionTableAdapter3 = new KarateV3.gsbDataSet5TableAdapters.karate_competitionTableAdapter();
            this.karate_kataTableAdapter1 = new KarateV3.gsbDataSet6TableAdapters.karate_kataTableAdapter();
            this.karate_clubTableAdapter2 = new KarateV3.gsbDataSet7TableAdapters.karate_clubTableAdapter();
            this.karate_utilisateurTableAdapter = new KarateV3.gsbDataSet8TableAdapters.karate_utilisateurTableAdapter();
            this.karate_utilisateur_typeTableAdapter = new KarateV3.gsbDataSet9TableAdapters.karate_utilisateur_typeTableAdapter();
            this.uslicenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usnomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usprenomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usemailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uspasswordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.utidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbnomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbadresseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbcodepostalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbvilleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateutilisateurtypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateutilisateurBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet8)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatekataBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet5)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatesecteurBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatekataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetKata)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetCompetitionMembre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetCompetitions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(957, 90);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(281, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Éditer mes informations et voir celles de mon club.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Compte";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::KarateV3.Properties.Resources.gsb_trans;
            this.pictureBox1.Location = new System.Drawing.Point(760, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 90);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.splitContainer1.Panel1.Controls.Add(this.button6);
            this.splitContainer1.Panel1.Controls.Add(this.button3);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Size = new System.Drawing.Size(957, 459);
            this.splitContainer1.SplitterDistance = 177;
            this.splitContainer1.TabIndex = 1;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Gainsboro;
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(0, 218);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(177, 109);
            this.button6.TabIndex = 3;
            this.button6.Tag = "3";
            this.button6.Text = "Clubs";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.buttonMenu_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gainsboro;
            this.button3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(0, 325);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(177, 134);
            this.button3.TabIndex = 2;
            this.button3.Text = "Déconnexion";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Gainsboro;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(0, 109);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(177, 109);
            this.button2.TabIndex = 1;
            this.button2.Tag = "2";
            this.button2.Text = "Compétitions";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.buttonMenu_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Gainsboro;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 109);
            this.button1.TabIndex = 0;
            this.button1.Tag = "1";
            this.button1.Text = "Utilisateurs";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.buttonMenu_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 459);
            this.panel2.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(-8, -5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(803, 480);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(795, 454);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Utilisateurs";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button13);
            this.panel8.Controls.Add(this.button14);
            this.panel8.Controls.Add(this.button15);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(592, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(200, 448);
            this.panel8.TabIndex = 1;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Gainsboro;
            this.button13.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(33, 91);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(124, 31);
            this.button13.TabIndex = 30;
            this.button13.Text = "Supprimer";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Gainsboro;
            this.button14.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.Location = new System.Drawing.Point(33, 54);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(124, 31);
            this.button14.TabIndex = 29;
            this.button14.Text = "Modifier";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Gainsboro;
            this.button15.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.Location = new System.Drawing.Point(33, 17);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(124, 31);
            this.button15.TabIndex = 28;
            this.button15.Text = "Ajouter";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.dataGridView3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(584, 448);
            this.panel7.TabIndex = 0;
            // 
            // dataGridView3
            // 
            this.dataGridView3.AllowUserToAddRows = false;
            this.dataGridView3.AllowUserToDeleteRows = false;
            this.dataGridView3.AllowUserToOrderColumns = true;
            this.dataGridView3.AutoGenerateColumns = false;
            this.dataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView3.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.uslicenceDataGridViewTextBoxColumn,
            this.usnomDataGridViewTextBoxColumn,
            this.usprenomDataGridViewTextBoxColumn,
            this.usemailDataGridViewTextBoxColumn,
            this.uspasswordDataGridViewTextBoxColumn,
            this.utidDataGridViewTextBoxColumn});
            this.dataGridView3.DataSource = this.karateutilisateurBindingSource;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.ReadOnly = true;
            this.dataGridView3.Size = new System.Drawing.Size(584, 448);
            this.dataGridView3.TabIndex = 0;
            // 
            // karateutilisateurtypeBindingSource
            // 
            this.karateutilisateurtypeBindingSource.DataMember = "karate_utilisateur_type";
            this.karateutilisateurtypeBindingSource.DataSource = this.gsbDataSet9;
            // 
            // gsbDataSet9
            // 
            this.gsbDataSet9.DataSetName = "gsbDataSet9";
            this.gsbDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karateutilisateurBindingSource
            // 
            this.karateutilisateurBindingSource.DataMember = "karate_utilisateur";
            this.karateutilisateurBindingSource.DataSource = this.gsbDataSet8;
            // 
            // gsbDataSet8
            // 
            this.gsbDataSet8.DataSetName = "gsbDataSet8";
            this.gsbDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(795, 454);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Compétitions";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.button7);
            this.panel5.Controls.Add(this.button11);
            this.panel5.Controls.Add(this.button12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(592, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 448);
            this.panel5.TabIndex = 25;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Gainsboro;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(33, 91);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(124, 31);
            this.button7.TabIndex = 27;
            this.button7.Text = "Supprimer";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Gainsboro;
            this.button11.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(33, 54);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(124, 31);
            this.button11.TabIndex = 26;
            this.button11.Text = "Modifier";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Gainsboro;
            this.button12.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(33, 17);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(124, 31);
            this.button12.TabIndex = 25;
            this.button12.Text = "Ajouter";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(585, 448);
            this.panel4.TabIndex = 21;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coidDataGridViewTextBoxColumn,
            this.codateDataGridViewTextBoxColumn,
            this.kaidDataGridViewTextBoxColumn,
            this.cbidDataGridViewTextBoxColumn1});
            this.dataGridView2.DataSource = this.karatecompetitionBindingSource3;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(585, 448);
            this.dataGridView2.TabIndex = 0;
            // 
            // coidDataGridViewTextBoxColumn
            // 
            this.coidDataGridViewTextBoxColumn.DataPropertyName = "co_id";
            this.coidDataGridViewTextBoxColumn.HeaderText = "Id";
            this.coidDataGridViewTextBoxColumn.Name = "coidDataGridViewTextBoxColumn";
            this.coidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codateDataGridViewTextBoxColumn
            // 
            this.codateDataGridViewTextBoxColumn.DataPropertyName = "co_date";
            this.codateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.codateDataGridViewTextBoxColumn.Name = "codateDataGridViewTextBoxColumn";
            this.codateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kaidDataGridViewTextBoxColumn
            // 
            this.kaidDataGridViewTextBoxColumn.DataPropertyName = "ka_id";
            this.kaidDataGridViewTextBoxColumn.DataSource = this.karatekataBindingSource1;
            this.kaidDataGridViewTextBoxColumn.DisplayMember = "ka_nom";
            this.kaidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.kaidDataGridViewTextBoxColumn.HeaderText = "Kata";
            this.kaidDataGridViewTextBoxColumn.Name = "kaidDataGridViewTextBoxColumn";
            this.kaidDataGridViewTextBoxColumn.ReadOnly = true;
            this.kaidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.kaidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.kaidDataGridViewTextBoxColumn.ValueMember = "ka_id";
            // 
            // karatekataBindingSource1
            // 
            this.karatekataBindingSource1.DataMember = "karate_kata";
            this.karatekataBindingSource1.DataSource = this.gsbDataSet6;
            // 
            // gsbDataSet6
            // 
            this.gsbDataSet6.DataSetName = "gsbDataSet6";
            this.gsbDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cbidDataGridViewTextBoxColumn1
            // 
            this.cbidDataGridViewTextBoxColumn1.DataPropertyName = "cb_id";
            this.cbidDataGridViewTextBoxColumn1.DataSource = this.karateclubBindingSource2;
            this.cbidDataGridViewTextBoxColumn1.DisplayMember = "cb_nom";
            this.cbidDataGridViewTextBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.cbidDataGridViewTextBoxColumn1.HeaderText = "Club";
            this.cbidDataGridViewTextBoxColumn1.Name = "cbidDataGridViewTextBoxColumn1";
            this.cbidDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cbidDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cbidDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cbidDataGridViewTextBoxColumn1.ValueMember = "cb_id";
            // 
            // karateclubBindingSource2
            // 
            this.karateclubBindingSource2.DataMember = "karate_club";
            this.karateclubBindingSource2.DataSource = this.gsbDataSet7;
            // 
            // gsbDataSet7
            // 
            this.gsbDataSet7.DataSetName = "gsbDataSet7";
            this.gsbDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karatecompetitionBindingSource3
            // 
            this.karatecompetitionBindingSource3.DataMember = "karate_competition";
            this.karatecompetitionBindingSource3.DataSource = this.gsbDataSet5;
            // 
            // gsbDataSet5
            // 
            this.gsbDataSet5.DataSetName = "gsbDataSet5";
            this.gsbDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(795, 454);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Clubs";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.button10);
            this.panel6.Controls.Add(this.button9);
            this.panel6.Controls.Add(this.button8);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(592, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 448);
            this.panel6.TabIndex = 21;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Gainsboro;
            this.button10.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(33, 91);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(124, 31);
            this.button10.TabIndex = 23;
            this.button10.Text = "Supprimer";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Gainsboro;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(33, 54);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(124, 31);
            this.button9.TabIndex = 22;
            this.button9.Text = "Modifier";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Gainsboro;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(33, 17);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(124, 31);
            this.button8.TabIndex = 21;
            this.button8.Text = "Ajouter";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(585, 448);
            this.panel3.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbidDataGridViewTextBoxColumn,
            this.cbnomDataGridViewTextBoxColumn,
            this.cbadresseDataGridViewTextBoxColumn,
            this.cbcodepostalDataGridViewTextBoxColumn,
            this.cbvilleDataGridViewTextBoxColumn,
            this.scidDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.karateclubBindingSource1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.Color.Gray;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(585, 448);
            this.dataGridView1.TabIndex = 0;
            // 
            // karatesecteurBindingSource
            // 
            this.karatesecteurBindingSource.DataMember = "karate_secteur";
            this.karatesecteurBindingSource.DataSource = this.gsbDataSet2;
            // 
            // gsbDataSet2
            // 
            this.gsbDataSet2.DataSetName = "gsbDataSet2";
            this.gsbDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karateclubBindingSource1
            // 
            this.karateclubBindingSource1.DataMember = "karate_club";
            this.karateclubBindingSource1.DataSource = this.gsbDataSet3;
            // 
            // gsbDataSet3
            // 
            this.gsbDataSet3.DataSetName = "gsbDataSet3";
            this.gsbDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karatekataBindingSource
            // 
            this.karatekataBindingSource.DataMember = "karate_kata";
            this.karatekataBindingSource.DataSource = this.gsbDataSetKata;
            // 
            // gsbDataSetKata
            // 
            this.gsbDataSetKata.DataSetName = "gsbDataSetKata";
            this.gsbDataSetKata.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karateclubBindingSource
            // 
            this.karateclubBindingSource.DataMember = "karate_club";
            this.karateclubBindingSource.DataSource = this.gsbDataSet;
            // 
            // gsbDataSet
            // 
            this.gsbDataSet.DataSetName = "gsbDataSet";
            this.gsbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karatecompetitionBindingSource1
            // 
            this.karatecompetitionBindingSource1.DataMember = "karate_competition";
            this.karatecompetitionBindingSource1.DataSource = this.gsbDataSetCompetitionMembre;
            // 
            // gsbDataSetCompetitionMembre
            // 
            this.gsbDataSetCompetitionMembre.DataSetName = "gsbDataSetCompetitionMembre";
            this.gsbDataSetCompetitionMembre.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karatecompetitionBindingSource
            // 
            this.karatecompetitionBindingSource.DataMember = "karate_competition";
            this.karatecompetitionBindingSource.DataSource = this.gsbDataSetCompetitions;
            // 
            // gsbDataSetCompetitions
            // 
            this.gsbDataSetCompetitions.DataSetName = "gsbDataSetCompetitions";
            this.gsbDataSetCompetitions.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karate_competitionTableAdapter
            // 
            this.karate_competitionTableAdapter.ClearBeforeFill = true;
            // 
            // karate_clubTableAdapter
            // 
            this.karate_clubTableAdapter.ClearBeforeFill = true;
            // 
            // karate_kataTableAdapter
            // 
            this.karate_kataTableAdapter.ClearBeforeFill = true;
            // 
            // karate_competitionTableAdapter1
            // 
            this.karate_competitionTableAdapter1.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(20, 66);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(137, 20);
            this.textBox1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 15);
            this.label3.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(174, 66);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(137, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(177, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(20, 185);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(291, 20);
            this.textBox3.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 15);
            this.label5.TabIndex = 5;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(20, 347);
            this.textBox4.Name = "textBox4";
            this.textBox4.PasswordChar = '*';
            this.textBox4.Size = new System.Drawing.Size(137, 20);
            this.textBox4.TabIndex = 7;
            this.textBox4.Tag = "mdp";
            this.textBox4.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 15);
            this.label6.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(20, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 15);
            this.label7.TabIndex = 9;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(20, 131);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(291, 20);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(20, 237);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(291, 20);
            this.textBox5.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(20, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 15);
            this.label8.TabIndex = 12;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(20, 290);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(137, 20);
            this.textBox6.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(20, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 15);
            this.label9.TabIndex = 14;
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(174, 290);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(137, 20);
            this.textBox7.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(174, 272);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 15);
            this.label10.TabIndex = 16;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gainsboro;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(174, 390);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(137, 31);
            this.button4.TabIndex = 10;
            this.button4.Text = "Sauvegarder";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(174, 347);
            this.textBox8.Name = "textBox8";
            this.textBox8.PasswordChar = '*';
            this.textBox8.Size = new System.Drawing.Size(137, 20);
            this.textBox8.TabIndex = 8;
            this.textBox8.Tag = "mdp";
            this.textBox8.UseSystemPasswordChar = true;
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(20, 396);
            this.textBox9.Name = "textBox9";
            this.textBox9.PasswordChar = '*';
            this.textBox9.Size = new System.Drawing.Size(137, 20);
            this.textBox9.TabIndex = 9;
            this.textBox9.UseSystemPasswordChar = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(174, 327);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 15);
            this.label23.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 376);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(117, 15);
            this.label18.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(138, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 15);
            this.label11.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(196, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 15);
            this.label12.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(138, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 15);
            this.label13.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(138, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 15);
            this.label14.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(138, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 15);
            this.label15.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(170, 15);
            this.label19.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(28, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 15);
            this.label20.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(28, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(90, 15);
            this.label21.TabIndex = 2;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(31, 193);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(308, 37);
            this.button5.TabIndex = 4;
            this.button5.Tag = "3";
            this.button5.Text = "Voir mes statistiques complètes";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonMenu_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(229, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(229, 80);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 6;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(229, 114);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(28, 144);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 15);
            this.label27.TabIndex = 8;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(229, 146);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 13);
            this.label26.TabIndex = 9;
            // 
            // karate_secteurTableAdapter
            // 
            this.karate_secteurTableAdapter.ClearBeforeFill = true;
            // 
            // karate_clubTableAdapter1
            // 
            this.karate_clubTableAdapter1.ClearBeforeFill = true;
            // 
            // gsbDataSet4
            // 
            this.gsbDataSet4.DataSetName = "gsbDataSet4";
            this.gsbDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // karatecompetitionBindingSource2
            // 
            this.karatecompetitionBindingSource2.DataMember = "karate_competition";
            this.karatecompetitionBindingSource2.DataSource = this.gsbDataSet4;
            // 
            // karate_competitionTableAdapter2
            // 
            this.karate_competitionTableAdapter2.ClearBeforeFill = true;
            // 
            // karate_competitionTableAdapter3
            // 
            this.karate_competitionTableAdapter3.ClearBeforeFill = true;
            // 
            // karate_kataTableAdapter1
            // 
            this.karate_kataTableAdapter1.ClearBeforeFill = true;
            // 
            // karate_clubTableAdapter2
            // 
            this.karate_clubTableAdapter2.ClearBeforeFill = true;
            // 
            // karate_utilisateurTableAdapter
            // 
            this.karate_utilisateurTableAdapter.ClearBeforeFill = true;
            // 
            // karate_utilisateur_typeTableAdapter
            // 
            this.karate_utilisateur_typeTableAdapter.ClearBeforeFill = true;
            // 
            // uslicenceDataGridViewTextBoxColumn
            // 
            this.uslicenceDataGridViewTextBoxColumn.DataPropertyName = "us_licence";
            this.uslicenceDataGridViewTextBoxColumn.HeaderText = "Licence";
            this.uslicenceDataGridViewTextBoxColumn.Name = "uslicenceDataGridViewTextBoxColumn";
            this.uslicenceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // usnomDataGridViewTextBoxColumn
            // 
            this.usnomDataGridViewTextBoxColumn.DataPropertyName = "us_nom";
            this.usnomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.usnomDataGridViewTextBoxColumn.Name = "usnomDataGridViewTextBoxColumn";
            this.usnomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // usprenomDataGridViewTextBoxColumn
            // 
            this.usprenomDataGridViewTextBoxColumn.DataPropertyName = "us_prenom";
            this.usprenomDataGridViewTextBoxColumn.HeaderText = "Prenom";
            this.usprenomDataGridViewTextBoxColumn.Name = "usprenomDataGridViewTextBoxColumn";
            this.usprenomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // usemailDataGridViewTextBoxColumn
            // 
            this.usemailDataGridViewTextBoxColumn.DataPropertyName = "us_email";
            this.usemailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.usemailDataGridViewTextBoxColumn.Name = "usemailDataGridViewTextBoxColumn";
            this.usemailDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uspasswordDataGridViewTextBoxColumn
            // 
            this.uspasswordDataGridViewTextBoxColumn.DataPropertyName = "us_password";
            this.uspasswordDataGridViewTextBoxColumn.HeaderText = "Password";
            this.uspasswordDataGridViewTextBoxColumn.Name = "uspasswordDataGridViewTextBoxColumn";
            this.uspasswordDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // utidDataGridViewTextBoxColumn
            // 
            this.utidDataGridViewTextBoxColumn.DataPropertyName = "ut_id";
            this.utidDataGridViewTextBoxColumn.DataSource = this.karateutilisateurtypeBindingSource;
            this.utidDataGridViewTextBoxColumn.DisplayMember = "ut_nom";
            this.utidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.utidDataGridViewTextBoxColumn.HeaderText = "Type";
            this.utidDataGridViewTextBoxColumn.Name = "utidDataGridViewTextBoxColumn";
            this.utidDataGridViewTextBoxColumn.ReadOnly = true;
            this.utidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.utidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.utidDataGridViewTextBoxColumn.ValueMember = "ut_id";
            // 
            // cbidDataGridViewTextBoxColumn
            // 
            this.cbidDataGridViewTextBoxColumn.DataPropertyName = "cb_id";
            this.cbidDataGridViewTextBoxColumn.HeaderText = "Id";
            this.cbidDataGridViewTextBoxColumn.Name = "cbidDataGridViewTextBoxColumn";
            this.cbidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cbnomDataGridViewTextBoxColumn
            // 
            this.cbnomDataGridViewTextBoxColumn.DataPropertyName = "cb_nom";
            this.cbnomDataGridViewTextBoxColumn.HeaderText = "Nom";
            this.cbnomDataGridViewTextBoxColumn.Name = "cbnomDataGridViewTextBoxColumn";
            this.cbnomDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cbadresseDataGridViewTextBoxColumn
            // 
            this.cbadresseDataGridViewTextBoxColumn.DataPropertyName = "cb_adresse";
            this.cbadresseDataGridViewTextBoxColumn.HeaderText = "Adresse";
            this.cbadresseDataGridViewTextBoxColumn.Name = "cbadresseDataGridViewTextBoxColumn";
            this.cbadresseDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cbcodepostalDataGridViewTextBoxColumn
            // 
            this.cbcodepostalDataGridViewTextBoxColumn.DataPropertyName = "cb_code_postal";
            this.cbcodepostalDataGridViewTextBoxColumn.HeaderText = "Code postal";
            this.cbcodepostalDataGridViewTextBoxColumn.Name = "cbcodepostalDataGridViewTextBoxColumn";
            this.cbcodepostalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cbvilleDataGridViewTextBoxColumn
            // 
            this.cbvilleDataGridViewTextBoxColumn.DataPropertyName = "cb_ville";
            this.cbvilleDataGridViewTextBoxColumn.HeaderText = "Ville";
            this.cbvilleDataGridViewTextBoxColumn.Name = "cbvilleDataGridViewTextBoxColumn";
            this.cbvilleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // scidDataGridViewTextBoxColumn
            // 
            this.scidDataGridViewTextBoxColumn.DataPropertyName = "sc_id";
            this.scidDataGridViewTextBoxColumn.DataSource = this.karatesecteurBindingSource;
            this.scidDataGridViewTextBoxColumn.DisplayMember = "sc_emplacement";
            this.scidDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.scidDataGridViewTextBoxColumn.HeaderText = "Secteur";
            this.scidDataGridViewTextBoxColumn.Name = "scidDataGridViewTextBoxColumn";
            this.scidDataGridViewTextBoxColumn.ReadOnly = true;
            this.scidDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.scidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.scidDataGridViewTextBoxColumn.ValueMember = "sc_id";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 549);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(973, 587);
            this.MinimumSize = new System.Drawing.Size(973, 587);
            this.Name = "AdminForm";
            this.Text = "Membre";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminForm_Closing);
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateutilisateurtypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateutilisateurBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet8)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatekataBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet5)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatesecteurBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatekataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetKata)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karateclubBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetCompetitionMembre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSetCompetitions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.karatecompetitionBindingSource2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private gsbDataSetCompetitions gsbDataSetCompetitions;
        private System.Windows.Forms.BindingSource karatecompetitionBindingSource;
        private gsbDataSetCompetitionsTableAdapters.karate_competitionTableAdapter karate_competitionTableAdapter;
        private gsbDataSet gsbDataSet;
        private System.Windows.Forms.BindingSource karateclubBindingSource;
        private gsbDataSetTableAdapters.karate_clubTableAdapter karate_clubTableAdapter;
        private gsbDataSetKata gsbDataSetKata;
        private System.Windows.Forms.BindingSource karatekataBindingSource;
        private gsbDataSetKataTableAdapters.karate_kataTableAdapter karate_kataTableAdapter;
        private gsbDataSetCompetitionMembre gsbDataSetCompetitionMembre;
        private System.Windows.Forms.BindingSource karatecompetitionBindingSource1;
        private gsbDataSetCompetitionMembreTableAdapters.karate_competitionTableAdapter karate_competitionTableAdapter1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private gsbDataSet2 gsbDataSet2;
        private System.Windows.Forms.BindingSource karatesecteurBindingSource;
        private gsbDataSet2TableAdapters.karate_secteurTableAdapter karate_secteurTableAdapter;
        private gsbDataSet3 gsbDataSet3;
        private System.Windows.Forms.BindingSource karateclubBindingSource1;
        private gsbDataSet3TableAdapters.karate_clubTableAdapter karate_clubTableAdapter1;
        private System.Windows.Forms.Panel panel4;
        private gsbDataSet4 gsbDataSet4;
        private System.Windows.Forms.BindingSource karatecompetitionBindingSource2;
        private gsbDataSet4TableAdapters.karate_competitionTableAdapter karate_competitionTableAdapter2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private gsbDataSet5 gsbDataSet5;
        private System.Windows.Forms.BindingSource karatecompetitionBindingSource3;
        private gsbDataSet5TableAdapters.karate_competitionTableAdapter karate_competitionTableAdapter3;
        private gsbDataSet6 gsbDataSet6;
        private System.Windows.Forms.BindingSource karatekataBindingSource1;
        private gsbDataSet6TableAdapters.karate_kataTableAdapter karate_kataTableAdapter1;
        private gsbDataSet7 gsbDataSet7;
        private System.Windows.Forms.BindingSource karateclubBindingSource2;
        private gsbDataSet7TableAdapters.karate_clubTableAdapter karate_clubTableAdapter2;
        private System.Windows.Forms.DataGridViewTextBoxColumn coidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn kaidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbidDataGridViewTextBoxColumn1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private gsbDataSet8 gsbDataSet8;
        private System.Windows.Forms.BindingSource karateutilisateurBindingSource;
        private gsbDataSet8TableAdapters.karate_utilisateurTableAdapter karate_utilisateurTableAdapter;
        private gsbDataSet9 gsbDataSet9;
        private System.Windows.Forms.BindingSource karateutilisateurtypeBindingSource;
        private gsbDataSet9TableAdapters.karate_utilisateur_typeTableAdapter karate_utilisateur_typeTableAdapter;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.DataGridViewTextBoxColumn uslicenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usnomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usprenomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usemailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn uspasswordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn utidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbnomDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbadresseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbcodepostalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cbvilleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn scidDataGridViewTextBoxColumn;

    }
}