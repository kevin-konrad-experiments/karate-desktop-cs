﻿using KarateV3.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Forms
{
    partial class AdminForm : Form
    {
        Utilisateur admin;

        public Utilisateur Admin
        {
            get { return admin; }
            set { admin = value; }
        }

        public AdminForm(Utilisateur u)
        {
            Admin = u;
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public AdminForm()
        {
            InitializeComponent();
        }

        private void AdminForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.OpenForms[0].Show();
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            TabPage tab = tabControl1.TabPages[Convert.ToInt32(button.Tag) - 1];

            switch (Convert.ToInt32(button.Tag))
            {
                case 1:
                    label1.Text = "Compte n°" + Admin.Licence;
                    label2.Text = "Éditer mes informations et voir celles de mon club.";
                    break;
                case 2:
                    label1.Text = "Compétitions";
                    label2.Text = "Voir les compétitions et en créer de nouvelles.";
                    break;
                case 3:
                    label1.Text = "Notes";
                    label2.Text = "Attribuer des notes aux participants pour les compétitions auxquelles vous avez été désigné comme jury.";
                    break;
            }

            tabControl1.SelectedTab = tab;
            tab.Show();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gsbDataSet9.karate_utilisateur_type' table. You can move, or remove it, as needed.
            this.karate_utilisateur_typeTableAdapter.Fill(this.gsbDataSet9.karate_utilisateur_type);
            // TODO: This line of code loads data into the 'gsbDataSet8.karate_utilisateur' table. You can move, or remove it, as needed.
            this.karate_utilisateurTableAdapter.Fill(this.gsbDataSet8.karate_utilisateur);
            // TODO: This line of code loads data into the 'gsbDataSet7.karate_club' table. You can move, or remove it, as needed.
            this.karate_clubTableAdapter2.Fill(this.gsbDataSet7.karate_club);
            // TODO: This line of code loads data into the 'gsbDataSet6.karate_kata' table. You can move, or remove it, as needed.
            this.karate_kataTableAdapter1.Fill(this.gsbDataSet6.karate_kata);
            // TODO: This line of code loads data into the 'gsbDataSet5.karate_competition' table. You can move, or remove it, as needed.
            this.karate_competitionTableAdapter3.Fill(this.gsbDataSet5.karate_competition);
            // TODO: This line of code loads data into the 'gsbDataSet4.karate_competition' table. You can move, or remove it, as needed.
            this.karate_competitionTableAdapter2.Fill(this.gsbDataSet4.karate_competition);
            // TODO: This line of code loads data into the 'gsbDataSet3.karate_club' table. You can move, or remove it, as needed.
            this.karate_clubTableAdapter1.Fill(this.gsbDataSet3.karate_club);
            // TODO: This line of code loads data into the 'gsbDataSet2.karate_secteur' table. You can move, or remove it, as needed.
            this.karate_secteurTableAdapter.Fill(this.gsbDataSet2.karate_secteur);
        
        }
    }
}
