﻿using KarateV3.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarateV3.Forms
{
    partial class MembreForm : Form
    {
        Membre membre;

        public Membre Membre
        {
            get { return membre; }
            set { membre = value; }
        }

        public MembreForm(Membre m)
        {
            Membre = m;
            InitializeComponent();
        }

        private void MembreForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.OpenForms[0].Show();
        }

        public MembreForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MembreForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gsbDataSetCompetitionMembre.karate_competition' table. You can move, or remove it, as needed.
            this.karate_competitionTableAdapter1.Fill(this.gsbDataSetCompetitionMembre.karate_competition);
            // TODO: This line of code loads data into the 'gsbDataSetKata.karate_kata' table. You can move, or remove it, as needed.
            this.karate_kataTableAdapter.Fill(this.gsbDataSetKata.karate_kata);
            // TODO: This line of code loads data into the 'gsbDataSet.karate_club' table. You can move, or remove it, as needed.
            this.karate_clubTableAdapter.Fill(this.gsbDataSet.karate_club);
            // TODO: This line of code loads data into the 'gsbDataSetCompetitions.karate_competition' table. You can move, or remove it, as needed.
            this.karate_competitionTableAdapter.Fill(this.gsbDataSetCompetitions.karate_competition);

            label1.Text = "Compte n°" + membre.Licence;
            textBox1.Text = membre.Nom;
            textBox2.Text = membre.Prenom;
            dateTimePicker1.Value = membre.DateNaissance;
            textBox3.Text = membre.Email;
            textBox5.Text = membre.Adresse;
            textBox6.Text = membre.CodePostal;
            textBox7.Text = membre.Ville;

            label11.Text = membre.Club.Nom;
            label15.Text = membre.Club.Adresse;
            label13.Text = membre.Club.CodePostal + " " + membre.Club.Ville;
            label12.Text = membre.Club.Secteur.Emplacement;

            List<Statistique> listeStats = membre.getStatsCompet();

            if (listeStats.Count > 0)
            {
                Statistique statsDerniereCompet = listeStats[0];

                label22.Text = statsDerniereCompet.Competition.Date.ToString("dd/MM/yyyy");
                label24.Text = statsDerniereCompet.Competition.Club.Nom;
                label25.Text = statsDerniereCompet.Competition.Kata.Nom;
                label26.Text = statsDerniereCompet.Moyenne.ToString();
            }
            else
            {
                label22.Text = "Aucune compétition";
                label24.Text = "Aucune compétition";
                label25.Text = "Aucune compétition";
                label26.Text = "Aucune compétition";
            }

            foreach (Statistique stat in listeStats)
                dataGridView2.Rows.Add(stat.Competition.Date.ToString("dd/MM/yyyy"), stat.Competition.Club.Nom, stat.Competition.Kata.Nom, stat.Moyenne);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bool vide = false;
            foreach (Control control in groupBox1.Controls)
            {
                if (control is TextBox)
                {
                    TextBox textBox = (TextBox)control;
                    if (textBox.Tag == null || (textBox.Tag != null && textBox.Tag.ToString() != "mdp"))
                    {
                        if (textBox.Text == null || textBox.Text == "")
                        {
                            vide = true;
                        }
                    }
                }
            }

            if (!vide)
            {
                    if (textBox9.Text == membre.Password)
                    {
                        membre.Nom = textBox1.Text;
                        membre.Prenom = textBox2.Text;
                        membre.DateNaissance = dateTimePicker1.Value;
                        membre.Email = textBox3.Text;
                        membre.Adresse = textBox5.Text;
                        membre.CodePostal = textBox6.Text;
                        membre.Ville = textBox7.Text;
                        if (textBox4.Text != null && textBox4.Text != "" && textBox8.Text != null && textBox8.Text != "")
                        {
                            if (textBox4.Text == textBox8.Text)
                            {
                                membre.Password = textBox4.Text;
                            }
                            else
                            {
                                Outils.errorBox("Le mot de passe et sa confirmation sont différents. Merci d'indiquer deux fois le même mot de passe.", "Erreur de mot de passe");
                                return;
                            }
                        }
                        membre.Update();
                        textBox4.Text = textBox8.Text = textBox9.Text = "";
                    }
                    else
                    {
                        Outils.errorBox("Mauvais mot de passe actuel. Merci de rentrer votre mot de passe actuel pour modifier vos informations.", "Erreur de mot de passe");
                    }
            }
            else
            {
                Outils.errorBox("Merci de remplir tous les champs (sauf les deux champs de nouveaux mot de passe qui sont facultatifs).", "Erreur de champs vides");
            }
        }

        private void buttonMenu_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            TabPage tab = tabControl1.TabPages[Convert.ToInt32(button.Tag) - 1];

            switch (Convert.ToInt32(button.Tag))
            {
                case 1:
                    label1.Text = "Compte n°" + membre.Licence;
                    label2.Text = "Éditer mes informations et voir celles de mon club.";
                    break;
                case 2:
                    label1.Text = "Compétitions";
                    label2.Text = "Voir les compétitions et s'inscrire à celles à venir.";
                    break;
                case 3:
                    label1.Text = "Statistiques";
                    label2.Text = "Voir vos statistiques sur les compétitions effectuées";
                    break;
            }

            tabControl1.SelectedTab = tab;
            tab.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;


            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                int CompetitionId = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                Competition competition = new Competition(CompetitionId);
                membre.Participer(competition);
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.karate_competitionTableAdapter1.FillBy(this.gsbDataSetCompetitionMembre.karate_competition);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.karate_competitionTableAdapter1.FillBy1(this.gsbDataSetCompetitionMembre.karate_competition);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.karate_competitionTableAdapter1.Fill(this.gsbDataSetCompetitionMembre.karate_competition);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByToolStripButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.karate_competitionTableAdapter1.FillBy(this.gsbDataSetCompetitionMembre.karate_competition);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
